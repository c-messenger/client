//
// Created by David on 2/15/2020.
//

#include "UserInput.h"

using namespace std;

int UserInput::getInt(const string& message, int from, int to) {
    cout << message << endl;

    int result;
    cin>>result;
    while (result < from || result > to) {
        cout << "You must enter value from " + to_string(from) + " to " + to_string(to) << endl;
        cin >> result;
    }

    return result;
}

string UserInput::getString(const string &message, bool required) {
    cout << message << endl;

    string result;

    getline(cin, result);
    while (required && result.empty()) {
//        cout << "You must enter valid value" << endl << flush;
        getline(cin, result);
    };

    return result;
}
