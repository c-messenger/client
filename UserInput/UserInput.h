//
// Created by David on 2/15/2020.
//

#ifndef MESSENGER_CLIENT_USERINPUT_H
#define MESSENGER_CLIENT_USERINPUT_H

#include <string>
#include <iostream>

using namespace std;

class UserInput {
public:
    int getInt(const string& message, int from, int to);
    string getString(const string& message, bool required = false);
};


#endif //MESSENGER_CLIENT_USERINPUT_H
