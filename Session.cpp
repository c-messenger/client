
#include "Session.h"


using namespace std;

Session::Session(boost::asio::io_service &io_service, boost::asio::ip::tcp::endpoint &endpoint) : socket_(io_service,
                                                                                                          endpoint.protocol()) {
    try {
        socket_.connect(endpoint);
    }
    catch (boost::system::system_error &e) {
        std::cout << "Error occured! Error code = " << e.code()
                  << ". Message: " << e.what();
    }

}


boost::asio::ip::tcp::socket &Session::socket() {
    return socket_;
}

void Session::start() {
    UserInput userInput;

    int registerOrLogin = userInput.getInt("1 - Login \n2 - Register", 1, 2);

    if (registerOrLogin == 1) {
        listenLogin();
    }
    if (registerOrLogin == 2) {
        listenRegister();
    }
}


void Session::writeMessage(const string &messageText) {
    boost::asio::write(socket_,
                             boost::asio::buffer(messageText, messageText.length()));
}

void Session::listenRegister() {
    UserInput userInput;

    string name = userInput.getString("Write your name", true);
    string email = userInput.getString("Write your email", true);
    string password = userInput.getString("Write your password", true);

    string request = Command::getCommand("register", {
            {"name",     name},
            {"email",    email},
            {"password", password}
    });

    removeData();
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
                            boost::bind(&Session::registerCallback, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

    boost::asio::write(socket_, boost::asio::buffer(request, request.length()));
}


void Session::registerCallback(const boost::system::error_code &error, size_t bytes_transferred) {
    if (!error) {
        if (data_[0] == 'e') {
            string res = Command::getResult(data_);
            cout << res << flush;
            listenRegister();
        } else if (data_[0] == 's') {
            cout << Command::getResult(data_) << endl;
            afterLogin();
        }
    } else {
        delete this;
    }
}

void Session::listenLogin() {
    UserInput userInput;

    string email = userInput.getString("Write your email", true);
    string password = userInput.getString("Write your password", true);

    string request = Command::getCommand("login", {
            {"email",    email},
            {"password", password}
    });

    removeData();
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
                            boost::bind(&Session::loginCallback, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

    boost::asio::write(socket_, boost::asio::buffer(request, request.length()));
}

void Session::loginCallback(const boost::system::error_code &error, size_t bytes_transferred) {
    if (!error) {
        if (data_[0] == 'e') {
            string res = Command::getResult(data_);
            cout << res << flush;
            listenLogin();
        } else if (data_[0] == 's') {
            cout << Command::getResult(data_) << endl;
            afterLogin();
        }
    } else {
        delete this;
    }
}


void Session::afterLogin() {
    listenGetOnlineUsersList();
}


void Session::listenGetOnlineUsersList() {
    string request = Command::getCommand("online_users", {
    });
    removeData();
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
                            boost::bind(&Session::getOnlineUsersListCallback, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

    boost::asio::write(socket_, boost::asio::buffer(request, request.length()));
}

void Session::getOnlineUsersListCallback(const boost::system::error_code &error, size_t bytes_transferred) {
    if (!error) {
        if (data_[0] == 'e') {
            string res = Command::getResult(data_);
            cout << res << flush;
            listenGetOnlineUsersList();
        } else if (data_[0] == 's') {
            onlineUsers += "--- ONLINE USERS ----\n" + Command::getResult(data_);

            reciveMessagesThread = thread (&Session::listenForNewMessages, this);
            handleUserInput();
        }
    } else {
        delete this;
    }
    reciveMessagesThread.join();
}


void Session::listenForNewMessages() {
    while (true) {
        removeData();
        socket_.read_some(boost::asio::buffer(data_, max_length));

        if (data_[0] == 'e') {
            string res = Command::getResult(data_);
            cout << res << flush;
        } else if (data_[0] == 'm') {
            usersMessages += Command::getResult(data_) + '\n';
        } else if (data_[0] == 'o') {
            onlineUsers = "--- ONLINE USERS ----\n" + Command::getResult(data_);
        }
        refreshPage();
    }
}

void Session::handleUserInput() {
    initscr();
    refreshPage();
    while (true) {
        refreshPage();

        char a = getch();
        switch (a) {
            case 24: // ctrl + x
                switch (messageInputState) {
                    case 1: //send input message
                        contactUserId = 0;
                        messageInputState = 0;
                        userInput = "";
                        break;
                }
                break;
            case 13: // enter
            case '\n': // enter
                switch (messageInputState) {
                    case 0: // get input user id
                        try {
                            contactUserId = stoi(userInput);
                            messageInputState = 1;
                        } catch (invalid_argument& e) {

                        }
                        userInput = "";

                        break;
                    case 1: //send input message
                        if (!userInput.empty())
                        {
                            string command = Command::getCommand("message", {
                                {"to", to_string(contactUserId)},
                                {"message", userInput},
                            });
                            writeMessage(command);

                            userInput = "";
                        }

                        break;
                }
                break;
            case 127: // backspace
                if (!userInput.empty()) {
                    userInput.pop_back();
                }
                break;
            default:
                userInput += keyname(a);
                break;
        }

        refreshPage();
    }
}

void Session::refreshPage() {
    string infoText;
    switch (messageInputState) {
        case 0:
            infoText = "Enter user id\nid: ";
            break;
        case 1:
            infoText = "Enter your message (Press ctrl + x to go back) \nmessage ("+to_string(contactUserId)+"): ";
            break;
    }

    clear();
    string res = onlineUsers + '\n' + usersMessages + '\n' + infoText + userInput;
    printw(res.c_str());
    refresh();
}

void Session::removeData() {
    for (int i = 0; i < max_length - 1; ++i)
        data_[i] = '\0';
}







