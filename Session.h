//
// Created by David on 2/14/2020.
//

#ifndef MESSENGER_SESSION_H
#define MESSENGER_SESSION_H

#include "Comand/Command.h"
#include "UserInput/UserInput.h"

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <string>
#include <iostream>
#include <cstdlib>
#include <thread>
#include <curses.h>

using namespace std;

class Session {
private:
    boost::asio::ip::tcp::socket socket_;
    enum {
        max_length = 1024
    };
    char data_[max_length]{};

    std::thread reciveMessagesThread;
    string onlineUsers;
    string usersMessages;
    string userInput;
    int contactUserId;
    int messageInputState = 0;
public:
    Session(boost::asio::io_service &io_service, boost::asio::ip::tcp::endpoint &endpoint);

    boost::asio::ip::tcp::socket &socket();

    void start();

private:
    void removeData();

    void writeMessage(const string &messageText);

    void listenRegister();
    void registerCallback(const boost::system::error_code &error, size_t bytes_transferred);

    void listenLogin();
    void loginCallback(const boost::system::error_code &error, size_t bytes_transferred);

    void listenForNewMessages();

    void afterLogin();

    void listenGetOnlineUsersList();
    void getOnlineUsersListCallback(const boost::system::error_code &error, size_t bytes_transferred);

    void handleUserInput();
    void refreshPage();
};


#endif //MESSENGER_SESSION_H
