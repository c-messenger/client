//
// Created by David on 2/15/2020.
//

#include "Command.h"

string Command::getCommand(const string &command, const map<string, string> &params) {
    string result = command + ' ';

    for (auto it = params.begin(); it != params.end(); ++it) {
        result += '-' + it->first + ' ' + '"' + it->second + '"' + ' ';
    }

    return result;
}

string Command::getResult(const string &command) {
    return command.substr(3, command.length()-2);
}
