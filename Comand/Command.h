//
// Created by David on 2/15/2020.
//

#ifndef MESSENGER_CLIENT_COMMAND_H
#define MESSENGER_CLIENT_COMMAND_H

#include <string>
#include <map>

using namespace std;

class Command {
public:
    static string getCommand(const string& command, const map<string, string>& params);

    static string getResult(const string& command);
};


#endif //MESSENGER_CLIENT_COMMAND_H
