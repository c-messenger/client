#include <iostream>
#include <boost/asio.hpp>
#include "Session.h"

using namespace std;

int main() {
/*

    initscr();
    printw("Hello World !!!");
    refresh();
    char a = getch(); endwin(); printf("KEY NAME : %s - %d\n", keyname(a), a);
*/

    std::string raw_ip_address = "127.0.0.1";
    unsigned short port_num = 8080;

    boost::asio::ip::tcp::endpoint
            endpoint(boost::asio::ip::address::from_string(raw_ip_address),
               port_num);

    boost::asio::io_service ioService;

    Session session(ioService, endpoint);

    session.start();

    ioService.run();

    return 0;
}
